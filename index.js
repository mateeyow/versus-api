'use strict'
const PORT = process.env.PORT || 3000;

require('./api/server').run(PORT);