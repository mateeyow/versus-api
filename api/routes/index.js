'use strict'
const UTIL = require('../services/util');

let express = require('express');
let Promise = require('bluebird');
let async = require('async');
let _ = require('lodash');

let model = require('../models');

Promise.promisifyAll(model);
let router = express.Router();

var getEmbedded = function (arr) {
  return new Promise((resolve, reject) => {
    let temp = [];
    arr.forEach(val => {temp.push(Object.keys(val.properties))});

    let properties = _.flatten(temp, true);
    
    model.findByNameAsync(properties)
    .then(function (data) {
      resolve(data);
    })
    .catch(function (err) {
      reject(err);
    });
  });
};

router.get('/:slug', function (req, res) {
  let slug = req.params.slug;

  model.findByNameUrlAsync([slug])
  .then(function (result) {
    let data = result[0];

    if (result.length === 0) {
      res.status(404);
      return res.json({status: 404, message: 'Item not found'});
    }

    async.waterfall([
      function (cb) {
        let properties = Object.keys(data.properties);
        model.findByNameAsync(properties)
        .then(function (propFirst) {
          cb(null, data, propFirst);
          return null;
        })
        .catch(function (err) {
          return cb(err, null);
        })
      },
      function (orig, propFirst, cb) {
        let arr = propFirst.filter(val => {return val.hasOwnProperty('properties')});
        if (arr.length !== 0) {
          getEmbedded(arr)
          .then(function (propSecond) {
            cb(null, orig, propFirst, propSecond);
            return null;
          })
          .catch(function (err) {
            UTIL.errorLog(`Error in getting data ${err}`);
            return cb('ERROR', null);
          })
        } else {
          return cb('End of data', orig, propFirst);
        }
      },
      function () {
        let orig = arguments[0];
        let propFirst = arguments[1];
        let propSecond = arguments[2];
        let cb = arguments[3];

        let arr = propSecond.filter(val => {return val.hasOwnProperty('properties')});

        if (arr.length !== 0) {
          getEmbedded(arr)
          .then(function (propLast) {
            return cb(null, orig, propFirst, propSecond, propLast);
          })
          .catch(function (err) {
            UTIL.errorLog(`Error in gettting data ${err}`);
            return cb('ERROR', null);
          })
        } else {
          return cb('End of data', orig, propFirst, propSecond);  
        }
      }
    ], function () {
      let arr = [];
      let length = arguments.length;
      let data = {base: arguments[1]};

      while (length > 2) {
        length--;
        arr.push(arguments[length]);
      }

      data.embedded = _.flatten(arr, true);
      res.json(data);
    })

    return null;
  })
  .catch(function (err) {
    res.status(500);
    return res.json({status: 500, message: 'Something went wrong.'});
  });
});

module.exports = router;