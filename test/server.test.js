'use strict'
let chai = require('chai');

chai.use(require('chai-http'));
let should = chai.should();
let agent = chai.request.agent('http://localhost:' + process.env.PORT);

describe('Api endpoint', function () {
  it('should send an HTTP status of 404', function (done) {
    agent.get('/test-route')
    .end(function (error, response) {
      response.should.have.status(404);
      done();
    });
  });

  it('should return an HTTP status of 202 for nexus-5 request', function (done) {
    agent.get('/nexus-5')
    .end(function (error, response) {
      response.should.have.status(200);
      done();
    });
  });

  it('should return an HTTP status of 202 for android-4-4-kitkat request', function (done) {
    agent.get('/android-4-4-kitkat')
    .end(function (error, response) {
      response.should.have.status(200);
      done();
    });
  });

  it('should return an HTTP status of 202 for qualcomm-snapdragon-800 request', function (done) {
    agent.get('/qualcomm-snapdragon-800')
    .end(function (error, response) {
      response.should.have.status(200);
      done();
    });
  });

  it('should return an HTTP status of 202 for qualcomm-adreno-330 request', function (done) {
    agent.get('/qualcomm-adreno-330')
    .end(function (error, response) {
      response.should.have.status(200);
      done();
    });
  });

  it('should return an HTTP status of 202 for qualcomm-krait-400 request', function (done) {
    agent.get('/qualcomm-krait-400')
    .end(function (error, response) {
      response.should.have.status(200);
      done();
    });
  });
});