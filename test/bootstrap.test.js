'use strict'
require('dotenv').config({path: `${__dirname}/.env.test`});

const PORT = process.env.PORT;

let server = require('../api/server');

before(function (done) {
  let app = server.run(PORT);
  done(null, app);
});