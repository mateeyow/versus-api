# Versus Application Test

All environment variables are stored on `.env` file. Normally we don't commit it but we'll make an excemption for now.

## Clone the repository

- `git clone git@github.com:mateeyow/versus-api.git`
- Run `npm install`
- Run `npm start`

### Go to `http:localhost:3000` to check if it is running

### Clone the [front-end](https://github.com/mateeyow/versus-front) repository to test out the api

## Test

Run `npm test` to test out the server