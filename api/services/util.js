'use strict'
let chalk = require('chalk');
let _ = require('lodash');

let success = chalk.blue;
let error = chalk.red;
let info = chalk.green;
let env = process.env.NODE_ENV;

let Util = function () {
  // Logs text with blue text color
  let _success = function (text) {
    if (env !== 'test') return console.log(success(text));
  };

  // Logs text with red text color
  let _error = function (text) {
    if (env !== 'test') return console.log(error(text));
  };

  let _info = function (text) {
    if (env !== 'test') return console.log(info(text));
  }

  let _assign = function () {
    let data = arguments[0]
    let prop = arguments[1];

    if (data instanceof Array) {
      let arr = [];
      data.forEach((v, idx, thisArr) => {
        let keys = v.properties && Object.keys(v.properties);

        if (keys) {
          for (let key in v.properties) {
            let value = v.properties[key];
            
            let result = _.find(prop, (val) => {return key === val.name});
            v.properties[key] = result;
            v.properties[key].value = value;
          }
        }

        arr.push(v);
      });

      data = arr;
    } else {
      let keys = Object.keys(data.properties);

      for (let key in data.properties) {
        let value = data.properties[key];
        // console.log(data.properties[key])
        let result = _.find(prop, (val) => {return key === val.name});
        data.properties[key] = result;
        data.properties[key].value = value;
      }
    }
      
    return data;
  };

  return {
    successLog:_success,
    errorLog: _error,
    infoLog: _info,
    assign: _assign
  };
}();

module.exports = Util;