'use strict'
const UTIL = require('./services/util');

let http = require('http');

let bodyParser = require('body-parser');
let express = require('express');

let app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.all('/*', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,OPTIONS');

  if (req.method === 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});

app.use('/', require('./routes'));
app.use(function (req, res, next) {
  res.status(404);
  return res.json({
    error: 'Not Found',
    message: 'URL not found'
  });
});

module.exports = {
  app: app,
  run: function (port) {
    http.createServer(app).listen(port, function (err) {
      UTIL.successLog(`Server is now running at port ${port}`);
    });
  }
}